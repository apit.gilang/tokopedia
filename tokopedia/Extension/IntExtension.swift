//
//  IntExtension.swift
//  tokopedia
//
//  Created by Apit on 1/28/19.
//  Copyright © 2019 Apit. All rights reserved.
//

import Foundation

extension Int {
    func toRupiah() -> String {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.numberStyle = .currency
        currencyFormatter.currencySymbol = "Rp "
        currencyFormatter.locale = Locale(identifier: "id_ID")
        return currencyFormatter.string(from: NSNumber(value: self))!
    }
    
    func toNumber() -> String {
        let currencyFormatter = NumberFormatter()
        currencyFormatter.numberStyle = .currency
        currencyFormatter.currencySymbol = ""
        currencyFormatter.locale = Locale(identifier: "id_ID")
        return currencyFormatter.string(from: NSNumber(value: self))!
    }
}
