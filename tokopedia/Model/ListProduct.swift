//
//  ListProduct.swift
//  tokopedia
//
//  Created by Apit on 1/27/19.
//  Copyright © 2019 Apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class ListProduct {
    var id: Int?
    var name: String?
    var imageUrl: String?
    var price: String?
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        id = json["id"].int
        name = json["name"].string
        imageUrl = json["image_uri"].string
        price = json["price"].string
    }
}
