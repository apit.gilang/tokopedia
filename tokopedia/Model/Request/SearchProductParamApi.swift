//
//  SearchProductParamApi.swift
//  tokopedia
//
//  Created by Apit on 1/27/19.
//  Copyright © 2019 Apit. All rights reserved.
//

import Foundation

class SearchProductParamApi {
    
    var query: String?
    var minPrice: String?
    var maxPrice: String?
    var whoSale: Bool?
    var official: Bool?
    var goodSeller: String?
    var startRow: String?
    var row: String?

    func buildForParameters() -> [String: AnyObject] {
        var parameters: [String: AnyObject] = [:]
        parameters["q"] = query as AnyObject
        parameters["pmin"] = minPrice as AnyObject
        parameters["pmax"] = maxPrice as AnyObject
        parameters["wholesale"] = whoSale as AnyObject
        parameters["official"] = official as AnyObject
        parameters["fshop"] = goodSeller as AnyObject
        parameters["start"] = startRow as AnyObject
        parameters["rows"] = row as AnyObject
        return parameters
    }
}
