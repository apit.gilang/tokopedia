//
//  Product.swift
//  tokopedia
//
//  Created by Apit on 1/29/19.
//  Copyright © 2019 Apit. All rights reserved.
//

import Foundation
import SwiftyJSON

class Product {
    var totalItem: Int?
    var listProduct: [ListProduct] = []
    
    init(json: JSON?) {
        guard let json = json else {
            return
        }
        totalItem = json["header"]["total_data"].int
        
        let arrProduct = json["data"].arrayValue
        for product in arrProduct {
            listProduct.append(ListProduct(json: product))
        }
    }
}
