//
//  Tokopedia.swift
//  tokopedia
//
//  Created by Apit on 1/27/19.
//  Copyright © 2019 Apit. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON
import AlamofireNetworkActivityLogger
import Reachability

typealias SuccessResponse = (JSON?) -> Void
typealias ErrorResponse = (String) -> Void

class Tokopedia {
    
    static let instance = Tokopedia()
    
    var alamofireManager: SessionManager = SessionManager.default
    var req: Request?
    
    init() {
        #if DEBUG
        NetworkActivityLogger.shared.level = .debug
        #else
        NetworkActivityLogger.shared.level = .off
        #endif
        self.setAFconfig()
    }
    
    func setAFconfig() {
        let configuration = URLSessionConfiguration.default
        configuration.timeoutIntervalForRequest = 30
        configuration.timeoutIntervalForResource = 30
        self.alamofireManager = Alamofire.SessionManager(configuration: configuration)
    }
    
    func request(_ request: URLRequestConvertible, success:@escaping SuccessResponse, error:@escaping ErrorResponse) {
        #if DEBUG
        NetworkActivityLogger.shared.startLogging()
        #endif
        req = alamofireManager.request(request).responseJSON { response in
            #if DEBUG
            if let errorMessage = response.result.error?.localizedDescription {
                print(errorMessage)
            }
            #endif
            
            let statusCode = response.response?.statusCode ?? 500
            if response.result.isFailure || 500 == response.response?.statusCode {
                if let reachability = Reachability(), reachability.connection == .none {
                    error(Response.MESSAGE_NOT_CONNECTED_INTERNET)
                } else {
                    error(Response.MESSAGE_SYSTEM_ERROR)
                }
            } else if let value = response.result.value {
                let jsonValue = JSON(value)
                if jsonValue["status"]["code"].int == 0 {
                    success(jsonValue)
                } else if statusCode >= 300 {
                    error(jsonValue["status"]["message"].stringValue)
                } else {
                    success(jsonValue)
                }
            } else {
                error(Response.MESSAGE_SYSTEM_ERROR)
            }
            
            self.invalidateAndConfigure()
        }
    }
    
    func invalidateAndConfigure() {
        #if DEBUG
        NetworkActivityLogger.shared.stopLogging()
        #endif
        self.alamofireManager.session.finishTasksAndInvalidate()
        self.setAFconfig()
    }
    
    class Response {
        static let MESSAGE_SYSTEM_ERROR = "Maaf, terjadi kesalahan sistem"
        static let MESSAGE_NOT_CONNECTED_INTERNET = "Tidak Ada Koneksi Internet"
    }
}
