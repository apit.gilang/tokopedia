//
//  SearchProductApi.swift
//  tokopedia
//
//  Created by Apit on 1/27/19.
//  Copyright © 2019 Apit. All rights reserved.
//

import Foundation
import Alamofire

enum ApiSearchProduct: URLRequestConvertible {
    
    case getListProduct(request: SearchProductParamApi)
    
    var path: String {
        switch self {
        case .getListProduct(_):
            return "search/v2.5/product"
        }
    }
    
    var method: HTTPMethod {
        switch self {
        case .getListProduct(_):
            return .get
        }
    }
    
    var parameter: [String: AnyObject] {
        switch self {
        case .getListProduct(let request):
            return request.buildForParameters()
        }
    }
    
    func asURLRequest() throws -> URLRequest {
        let url = try ApiConstants.BASE_URL.asURL()
        var urlRequest = URLRequest(url: url.appendingPathComponent(self.path))
        urlRequest.httpMethod = method.rawValue
        return try URLEncoding.default.encode(urlRequest, with: self.parameter)
    }
}
