//
//  Configurator+ShopType.swift
//  tokopedia
//
//  Created by Apit on 1/28/19.
//  Copyright © 2019 Apit. All rights reserved.
//

import UIKit

extension Configurator {
    func config(withShopTypeViewController vc: ShopTypeViewController) {
        let present = ShopTypePresent()
        present.response = vc
        vc.request = present
    }
}   
