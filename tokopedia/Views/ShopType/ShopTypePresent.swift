//
//  ShopTypePresent.swift
//  tokopedia
//
//  Created by Apit on 1/28/19.
//  Copyright © 2019 Apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol ShopTypeRequest: class {
    // TODO: Declare request methods
}

protocol ShopTypeResponse: class {
    // TODO: Declare response methods
}

class ShopTypePresent: ShopTypeRequest {

    weak var response: ShopTypeResponse?
    
    // TODO: Implement request methods
}
