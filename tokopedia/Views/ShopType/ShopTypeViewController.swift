//
//  ShopTypeViewController.swift
//  tokopedia
//
//  Created by Apit on 1/28/19.
//  Copyright © 2019 Apit. All rights reserved.
//

import Foundation
import UIKit

protocol ShopTypeDelegate {
    func applyShop(withSelectedType types: [String])
}

class ShopTypeViewController: ViewController {
    
    // MARK: Properties
    
    let shopType = ["Gold Merchant", "Official Store"]
    
    @IBOutlet weak var tableView: UITableView!
    
    var selectedType: [String] = []
    var delegate: ShopTypeDelegate?
    var request: ShopTypeRequest?
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Configurator.instance.config(withShopTypeViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        tableView.register(UINib(nibName: "ShopTypeTableViewCell", bundle: nil), forCellReuseIdentifier: "ShopTypeTableViewCell")
        tableView.delegate = self
        tableView.dataSource = self
        tableView.backgroundColor = UIColor.clear
    }
    
    @IBAction func onResetButtonPressed(_ sender: Any) {
        for row in 0..<shopType.count {
            let indexPath = IndexPath(row: row, section: 0)
            let cell = tableView.dequeueReusableCell(withIdentifier: "ShopTypeTableViewCell", for: indexPath) as! ShopTypeTableViewCell
            cell.selectedView.backgroundColor = UIColor.white
        }
        selectedType.removeAll()
        tableView.reloadData()
    }
    
    @IBAction func onCloseButtonPressed(_ sender: Any) {
        dismiss(animated: true)
    }
    
    @IBAction func onApplyButtonPressed(_ sender: Any) {
        if selectedType.count > 0 {
            self.dismiss(animated: true) {
                self.delegate?.applyShop(withSelectedType: self.selectedType)
            }
        }
    }
}

extension ShopTypeViewController: UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shopType.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "ShopTypeTableViewCell", for: indexPath) as! ShopTypeTableViewCell
        cell.shopTypeLabel.text = shopType[indexPath.row]
        return cell
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 55
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! ShopTypeTableViewCell
        cell.selectedView.backgroundColor = UIColor.init(red: 45/255, green: 180/255, blue: 88/255, alpha: 1.0)
        selectedType.append(shopType[indexPath.row])
    }
    
    func tableView(_ tableView: UITableView, didDeselectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! ShopTypeTableViewCell
        cell.selectedView.backgroundColor = UIColor.white
        selectedType.remove(at: indexPath.row)
    }
}

extension ShopTypeViewController: ShopTypeResponse {
    // TODO: implement response methods
}
