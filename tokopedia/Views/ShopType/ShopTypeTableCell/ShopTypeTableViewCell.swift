//
//  ShopTypeTableViewCell.swift
//  tokopedia
//
//  Created by Apit on 1/28/19.
//  Copyright © 2019 Apit. All rights reserved.
//

import UIKit

class ShopTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var selectedView: UIView!
    @IBOutlet weak var shopTypeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        selectionStyle = .none
        selectedView.layer.borderColor = UIColor.lightGray.cgColor
        selectedView.layer.borderWidth = 1
    }
}
