//
//  FilterProductPresent.swift
//  tokopedia
//
//  Created by Apit on 1/28/19.
//  Copyright © 2019 Apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol FilterProductRequest: class {
    // TODO: Declare request methods
}

protocol FilterProductResponse: class {
    // TODO: Declare response methods
}

class FilterProductPresent: FilterProductRequest {

    weak var response: FilterProductResponse?
    
    // TODO: Implement request methods
}
