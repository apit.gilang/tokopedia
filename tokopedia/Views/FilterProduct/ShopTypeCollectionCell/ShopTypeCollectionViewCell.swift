//
//  ShopTypeCollectionViewCell.swift
//  tokopedia
//
//  Created by Apit on 1/28/19.
//  Copyright © 2019 Apit. All rights reserved.
//

import UIKit

class ShopTypeCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var borderView: UIView!
    @IBOutlet weak var removeShopTypeView: UIView!
    @IBOutlet weak var removeShopTypeButton: UIButton!
    @IBOutlet weak var typeLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        borderView.layer.cornerRadius = 25
        borderView.layer.borderColor = UIColor.lightGray.cgColor
        borderView.layer.borderWidth = 1
        removeShopTypeView.layer.cornerRadius = 25
        removeShopTypeView.layer.borderColor = UIColor.lightGray.cgColor
        removeShopTypeView.layer.borderWidth = 1
    }

}
