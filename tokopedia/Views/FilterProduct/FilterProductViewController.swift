//
//  FilterProductViewController.swift
//  tokopedia
//
//  Created by Apit on 1/28/19.
//  Copyright © 2019 Apit. All rights reserved.
//

import Foundation
import UIKit
import RangeSeekSlider

protocol FilterProductDelegate {
    func applyFilter(withMinPrice minPrice: String,
                     withMaxPrice maxPrice: String,
                     withIsWhoSale isWhoSale: Bool,
                     withShopType shopType: String,
                     withIsOfficialSeller isOfficialSeller: Bool)
}

class FilterProductViewController: ViewController {
    
    // MARK: Properties
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var collectionViewHeight: NSLayoutConstraint!
    @IBOutlet weak var minimumPriceLabel: UILabel!
    @IBOutlet weak var maximumPriceLabel: UILabel!
    @IBOutlet weak var whoSaleButton: UISwitch!
    @IBOutlet weak var rangePriceSlider: RangeSeekSlider!
    
    var minValue = 100
    var maxValue = 10000000
    var filterDelegate: FilterProductDelegate?
    var request: FilterProductRequest?
    var selectedShopType: [String] = []
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Configurator.instance.config(withFilterProductViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupDefaultFilter()
        collectionView.register(UINib(nibName: "ShopTypeCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ShopTypeCollectionViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        rangePriceSlider.delegate = self
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if SegueConstants.FilterProduct.openShopTypePage == segue.identifier {
            if let vc = segue.destination as? ShopTypeViewController {
                vc.delegate = self
            }
        }
    }
    
    @IBAction func onResetFilterPressed(_ sender: Any) {
        setupDefaultFilter()
    }
    
    @IBAction func onCloseFilterPressed(_ sender: Any) {
        self.dismiss(animated: true)
    }
    
    @IBAction func onShopTypeButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: SegueConstants.FilterProduct.openShopTypePage, sender: nil)
    }
    
    @IBAction func onApplyButtonPressed(_ sender: Any) {
        self.dismiss(animated: true) {
            self.filterDelegate?.applyFilter(withMinPrice: "\(self.minValue)", withMaxPrice: "\(self.maxValue)", withIsWhoSale: self.whoSaleButton.isOn, withShopType: self.getShopType(), withIsOfficialSeller: self.getIsOfficial())
        }
    }
    
    func getShopType() -> String {
        var result = "0"
        for type in selectedShopType {
            if type == "Good Merchant" {
                result = "2"
            }
        }
        return result
    }
    
    func getIsOfficial() -> Bool {
        for type in selectedShopType {
            if type == "Official Store" {
                return true
            }
        }
        return false
    }
    
    func setupDefaultFilter() {
        minimumPriceLabel.text = 100.toRupiah()
        maximumPriceLabel.text = 10000000.toRupiah()
        rangePriceSlider.minValue = 0
        rangePriceSlider.maxValue = 100
        whoSaleButton.isOn = false
        collectionViewHeight.constant = 0
    }
    
    @objc func removeShopType(withIndex sender: UIButton) {
        selectedShopType.remove(at: sender.tag)
        collectionView.reloadData()
    }
}

extension FilterProductViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return selectedShopType.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ShopTypeCollectionViewCell", for: indexPath) as! ShopTypeCollectionViewCell
        cell.typeLabel.text = selectedShopType[indexPath.item]
        cell.removeShopTypeButton.tag = indexPath.row
        cell.removeShopTypeButton.addTarget(self, action: #selector(removeShopType(withIndex:)), for: .touchUpInside)
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: 250, height: 50)
    }
}

extension FilterProductViewController: RangeSeekSliderDelegate {
    func rangeSeekSlider(_ slider: RangeSeekSlider, didChange minValue: CGFloat, maxValue: CGFloat) {
        self.minValue = Int(Int(minValue) * 100000)
        self.maxValue = Int(Int(maxValue) * 100000)
        minimumPriceLabel.text = Int(Int(minValue) * 100000).toRupiah()
        maximumPriceLabel.text = Int(Int(maxValue) * 100000).toRupiah()
    }
}

extension FilterProductViewController: ShopTypeDelegate {
    func applyShop(withSelectedType types: [String]) {
        selectedShopType = types
        collectionViewHeight.constant = 50
        collectionView.reloadData()
    }
}

extension FilterProductViewController: FilterProductResponse {
    // TODO: implement response methods
}
