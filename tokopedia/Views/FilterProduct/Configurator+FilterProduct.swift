//
//  Configurator+FilterProduct.swift
//  tokopedia
//
//  Created by Apit on 1/28/19.
//  Copyright © 2019 Apit. All rights reserved.
//

import UIKit

extension Configurator {
    func config(withFilterProductViewController vc: FilterProductViewController) {
        let present = FilterProductPresent()
        present.response = vc
        vc.request = present
    }
}   
