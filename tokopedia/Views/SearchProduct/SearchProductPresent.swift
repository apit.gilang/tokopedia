//
//  SearchProductPresent.swift
//  tokopedia
//
//  Created by Apit on 1/27/19.
//  Copyright © 2019 Apit. All rights reserved.
//

import Foundation
import SwiftyJSON

protocol SearchProductRequest: class {
    func getListProduct(withRequest request: SearchProductParamApi)
}

protocol SearchProductResponse: class {
    func displayProduct(withProduct product: Product)
    func displayErrorGetProduct(withMessage message: String)
}

class SearchProductPresent: SearchProductRequest {

    weak var response: SearchProductResponse?
    
    func getListProduct(withRequest request: SearchProductParamApi) {
        Tokopedia.instance.request(ApiSearchProduct.getListProduct(request: request), success: { (json) in
            self.response?.displayProduct(withProduct: Product(json: json))
        }) { (error) in
            self.response?.displayErrorGetProduct(withMessage: error)
        }
    }
}
