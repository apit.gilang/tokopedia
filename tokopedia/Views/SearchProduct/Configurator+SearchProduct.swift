//
//  Configurator+SearchProduct.swift
//  tokopedia
//
//  Created by Apit on 1/27/19.
//  Copyright © 2019 Apit. All rights reserved.
//

import UIKit

extension Configurator {
    func config(withSearchProductViewController vc: SearchProductViewController) {
        let present = SearchProductPresent()
        present.response = vc
        vc.request = present
    }
}   
