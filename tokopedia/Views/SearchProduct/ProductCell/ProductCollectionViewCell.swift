//
//  ProductCollectionViewCell.swift
//  tokopedia
//
//  Created by Apit on 1/27/19.
//  Copyright © 2019 Apit. All rights reserved.
//

import UIKit
import SDWebImage

class ProductCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var productImage: UIImageView!
    @IBOutlet weak var productNameLabel: UILabel!
    @IBOutlet weak var productPriceLabel: UILabel!
    
    func setupProduct(withProduct product: ListProduct) {
        if let imageUrl = URL(string: product.imageUrl ?? "") {
            productImage.sd_setImage(with: imageUrl)
        }
        productNameLabel.text = product.name
        productPriceLabel.text = product.price
    }

}
