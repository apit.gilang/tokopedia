//
//  SearchProductViewController.swift
//  tokopedia
//
//  Created by Apit on 1/27/19.
//  Copyright © 2019 Apit. All rights reserved.
//

import Foundation
import UIKit

class SearchProductViewController: ViewController {
    
    // MARK: Properties
    @IBOutlet weak var collectionView: UICollectionView!
    
    var product: Product?
    var isFilter = false
    var listProduct: [ListProduct] = []
    var searchProductParam = SearchProductParamApi()
    var request: SearchProductRequest?
    
    // MARK: Lifecycle
    
    override func awakeFromNib() {
        super.awakeFromNib()
        Configurator.instance.config(withSearchProductViewController: self)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.register(UINib(nibName: "ProductCollectionViewCell", bundle: nil), forCellWithReuseIdentifier: "ProductCollectionViewCell")
        collectionView.delegate = self
        collectionView.dataSource = self
        
        searchProductParam.query = "samsung"
        searchProductParam.minPrice = "10000"
        searchProductParam.maxPrice = "100000"
        searchProductParam.whoSale = true
        searchProductParam.official = true
        searchProductParam.goodSeller = "2"
        searchProductParam.startRow = "0"
        searchProductParam.row = "10"
        
        loadListProduct()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if SegueConstants.SearchProduct.openFilterPage == segue.identifier {
            if let vc = segue.destination as? FilterProductViewController {
                vc.filterDelegate = self
            }
        }
    }
    
    @IBAction func onFilterButtonPressed(_ sender: Any) {
        self.performSegue(withIdentifier: SegueConstants.SearchProduct.openFilterPage, sender: nil)
    }
    
    func loadListProduct() {
        request?.getListProduct(withRequest: searchProductParam)
    }
}

extension SearchProductViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return listProduct.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ProductCollectionViewCell", for: indexPath) as! ProductCollectionViewCell
        cell.setupProduct(withProduct: listProduct[indexPath.item])
        if listProduct.count - 1 == indexPath.item {
            searchProductParam.startRow = "\(listProduct.count + 1)"
            loadListProduct()
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: (self.collectionView.frame.size.width / 2), height: 250)
    }
}

extension SearchProductViewController: FilterProductDelegate {
    func applyFilter(withMinPrice minPrice: String, withMaxPrice maxPrice: String, withIsWhoSale isWhoSale: Bool, withShopType shopType: String, withIsOfficialSeller isOfficialSeller: Bool) {
        self.listProduct.removeAll()
        self.isFilter = true
        searchProductParam = SearchProductParamApi()
        searchProductParam.query = "samsung"
        searchProductParam.minPrice = minPrice
        searchProductParam.maxPrice = maxPrice
        searchProductParam.whoSale = isWhoSale
        searchProductParam.official = isOfficialSeller
        searchProductParam.goodSeller = shopType
        searchProductParam.startRow = "0"
        searchProductParam.row = "10"
        loadListProduct()
    }
}

extension SearchProductViewController: SearchProductResponse {
    func displayProduct(withProduct product: Product) {
        if !isFilter {
            var indexPaths = [IndexPath]()
            var index = self.listProduct.count
            
            for item in product.listProduct {
                let indexPath = IndexPath(row: index, section: 0)
                index = index + 1
                
                indexPaths.append(indexPath)
                self.listProduct.append(item)
            }
            self.collectionView.performBatchUpdates({
                self.collectionView.insertItems(at: indexPaths)
            }, completion: nil)
        } else {
            self.isFilter = false
            self.listProduct = product.listProduct
            self.collectionView.reloadData()
        }
    }
    
    func displayErrorGetProduct(withMessage message: String) {
        showAlert(withMessage: message)
    }
}
