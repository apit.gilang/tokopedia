//
//  tokopediaTests.swift
//  tokopediaTests
//
//  Created by Apit on 1/27/19.
//  Copyright © 2019 Apit. All rights reserved.
//

import XCTest
import SwiftyJSON
@testable import tokopedia

class tokopediaTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }
    
    func testInit_ValidJson() {
        let productModel = Product(json: build(withFilename: "200_search_product_list"))
        XCTAssertNotNil(productModel)
    }
}

extension tokopediaTests {    
    func build(withFilename filename: String, withExtension fileExtension: String = "json") -> JSON? {
        if let file = Bundle(for: type(of: self)).url(forResource: filename, withExtension: fileExtension) {
            let data = try! Data(contentsOf: file)
            let dataJson = try! JSON(data: data)
            return dataJson
        }
        return nil
    }
}
